package za.co.peruzal.praat.model;

/**
 * Created by joseph on 7/14/15.
 */
public class ChatMessage {
    private String username;
    private String message;
    boolean isSelf;

    public ChatMessage(String username, String message, boolean isSelf) {
        this.username = username;
        this.message = message;
        this.isSelf = isSelf;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setIsSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }
}
