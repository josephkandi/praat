package za.co.peruzal.praat.app;

import android.app.Application;
import android.content.Intent;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

import za.co.peruzal.praat.service.ChatService;
import za.co.peruzal.praat.utils.Constants;

/**
 * Created by joseph on 7/16/15.
 */
public class PraatApp extends Application {
    private Socket socket;


    public Socket getSocket() {
        if (socket == null){
            try {
                socket = IO.socket(Constants.Socket.URL);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return socket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            socket = IO.socket(Constants.Socket.URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        startService(new Intent(this, ChatService.class));

    }
}
