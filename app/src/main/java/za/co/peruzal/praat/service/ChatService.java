package za.co.peruzal.praat.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import za.co.peruzal.praat.app.PraatApp;
import za.co.peruzal.praat.database.DatabaseHelper;
import za.co.peruzal.praat.model.ChatMessage;
import za.co.peruzal.praat.utils.Constants;

public class ChatService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = ChatService.class.getSimpleName();
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;
    private Socket socket;
    private SharedPreferences sharedPreferences;
    private String username;
    private PraatApp praatApp;

    public ChatService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        praatApp = ((PraatApp)getApplication());
        socket = praatApp.getSocket();
        databaseHelper = new DatabaseHelper(this);
        database = databaseHelper.getWritableDatabase();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);


        username = sharedPreferences.getString("username", "Bob");

        socket.on(Constants.Socket.NEW_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject object = (JSONObject)args[0];
                try {
                    String username = object.getString("username");
                    String message = object.getString("message");
                    ChatMessage chatMessage = new ChatMessage(username, message, false);

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Constants.Database.COLUMN_USERNAME, username);
                    contentValues.put(Constants.Database.COLUMN_MESSAGE, message);
                    contentValues.put(Constants.Database.COLUMN_IS_SELF, false);

                      long id =  database.insert(Constants.Database.TABLE_NAME, null, contentValues);
                    if (id != -1){
                        Log.d(TAG, "row inserted");
                    }

                    Intent intent = new Intent(Constants.Socket.NEW_MESSAGE_RECEIVED);
//                    intent.putExtra("username", username);
//                    intent.putExtra("message", message);

                    sendBroadcast(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        socket.connect();
        socket.emit("add user", username);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        socket.disconnect();
        socket.off(Constants.Socket.NEW_MESSAGE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStart");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("username")){
            username = sharedPreferences.getString("username", "Bob");
            socket.disconnect();

            Log.d(TAG, "username changed");

            socket.connect();
            socket.emit("add user", username);
        }
    }
}
