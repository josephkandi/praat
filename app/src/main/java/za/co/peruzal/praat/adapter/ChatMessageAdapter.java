package za.co.peruzal.praat.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import za.co.peruzal.praat.R;
import za.co.peruzal.praat.utils.Constants;

/**
 * Created by joseph on 7/17/15.
 */
public class ChatMessageAdapter extends SimpleCursorAdapter {
    private static int ROW_CHAT_RIGHT = 0;
    private static int ROW_CHAT_ME = 1;
    private static int ROW_CHAT_MIDDLE = 2;
    static int[] to =  { R.id.username, R.id.message};
    static String[] from = { Constants.Database.COLUMN_USERNAME, Constants.Database.COLUMN_MESSAGE, Constants.Database.COLUMN_IS_SELF,Constants.Database.COLUMN_ID };
    public ChatMessageAdapter(Context context, Cursor c) {
        super(context, 0, c, from, to, 0);
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }


    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        return getItemViewType(cursor);
    }

    private int getItemViewType(Cursor cursor){
        int item = cursor.getInt(cursor.getColumnIndex(Constants.Database.COLUMN_IS_SELF));
        switch (item){
            case 0:
                return ROW_CHAT_RIGHT;
            case 1:
                return ROW_CHAT_ME;
            case 2:
                return ROW_CHAT_MIDDLE;
            default:
                return ROW_CHAT_ME;

        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        //super.newView(context, cursor, parent);
        ViewHolder viewHolder;
        View view = null;
        if (view == null){
            int item_type = cursor.getInt(cursor.getColumnIndex(Constants.Database.COLUMN_IS_SELF));
            switch (item_type){
                case 0:
                    view = View.inflate(context,R.layout.row_chat_right,null);
                    break;
                case 1:
                    view = View.inflate(context,R.layout.row_chat_left,null);
                    break;
                case 2:
                    view = View.inflate(context, R.layout.row_chat_middle,null);
                    break;
            }
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)view.getTag();
        }

        String username = cursor.getString(cursor.getColumnIndex(Constants.Database.COLUMN_USERNAME));
        String message = cursor.getString(cursor.getColumnIndex(Constants.Database.COLUMN_MESSAGE));

        viewHolder.usernameTextView.setText(username);
        viewHolder.messageTextView.setText(message);

        return view;
    }

    static class ViewHolder{
        @Bind(R.id.username) TextView usernameTextView;
        @Bind(R.id.message) TextView messageTextView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
