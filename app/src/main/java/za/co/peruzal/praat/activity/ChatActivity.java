package za.co.peruzal.praat.activity;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.Socket;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import za.co.peruzal.praat.R;
import za.co.peruzal.praat.adapter.ChatMessageAdapter;
import za.co.peruzal.praat.app.PraatApp;
import za.co.peruzal.praat.database.DatabaseHelper;
import za.co.peruzal.praat.utils.Constants;

public class ChatActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Bind(R.id.edChat) EditText chatMessageEditText;
    @Bind(R.id.listView) ListView listView;

    private static final String TAG = ChatActivity.class.getSimpleName();
    private ChatMessageAdapter chatMessageAdapter;
    private Cursor cursor;
    private  SQLiteDatabase database;
    private Socket socket;
    private String username;
    private SharedPreferences sharedPreferences;
    private PraatApp praatApp;
    private ChatMessageReceiver chatMessageReceiver  = new ChatMessageReceiver();

    String[] FROM = { Constants.Database.COLUMN_USERNAME, Constants.Database.COLUMN_MESSAGE, Constants.Database.COLUMN_IS_SELF, Constants.Database.COLUMN_ID };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        praatApp = ((PraatApp)getApplication());
        socket = praatApp.getSocket();
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        database = databaseHelper.getWritableDatabase();

        cursor = database.query(Constants.Database.TABLE_NAME, FROM,null,null,null,null,null);

        chatMessageAdapter = new ChatMessageAdapter(this, cursor);
        listView.setAdapter(chatMessageAdapter);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        //TODO - Make sure thee intent contains the username
        username = sharedPreferences.getString("username","");
        if (TextUtils.isEmpty(username)){
            startActivity(new Intent(this, PrefsActivity.class));
            Toast.makeText(this, "Provide Nickname", Toast.LENGTH_SHORT).show();
        }

        chatMessageEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND){
                    sendChatMessage();
                }
                return true;
            }
        });

    }


    @OnClick(R.id.btnSend) public void sendChatMessage(){
            String message = chatMessageEditText.getText().toString().trim();
            socket.emit("new message", message );

            ContentValues values = new ContentValues();
            values.put(Constants.Database.COLUMN_USERNAME, username);
            values.put(Constants.Database.COLUMN_MESSAGE, message);
            values.put(Constants.Database.COLUMN_IS_SELF, true);

            long id = database.insert(Constants.Database.TABLE_NAME,null,values);
            if (id != -1){
                Log.d(TAG, "Chat not inserted");
            }
            cursor.requery();
            chatMessageAdapter.notifyDataSetChanged();
            chatMessageEditText.setText("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        socket.connect();
        registerReceiver(chatMessageReceiver, new IntentFilter(Constants.Socket.NEW_MESSAGE_RECEIVED));
        Log.d(TAG, "Registering receiver");
    }

    @Override
    protected void onPause() {
        super.onPause();
        socket.disconnect();
        unregisterReceiver(chatMessageReceiver);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("username")){
            username = sharedPreferences.getString("username", "");
        }
    }


    private class ChatMessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            cursor.requery();
            chatMessageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, PrefsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void vibrate(){
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(1000);



    }
}
