package za.co.peruzal.praat.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import za.co.peruzal.praat.service.ChatService;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, ChatService.class));
    }
}
