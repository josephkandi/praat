package za.co.peruzal.praat.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import za.co.peruzal.praat.utils.Constants;

/**
 * Created by joseph on 7/16/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    public DatabaseHelper(Context context) {
        super(context, Constants.Database.DATABASE_NAME, null, Constants.Database.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlQuery = String.format("CREATE TABLE %s(%s integer primary key autoincrement, %s text, %s text, %s integer)",
                Constants.Database.TABLE_NAME, Constants.Database.COLUMN_ID, Constants.Database.COLUMN_USERNAME, Constants.Database.COLUMN_MESSAGE, Constants.Database.COLUMN_IS_SELF);
        Log.d(TAG, sqlQuery);
        db.execSQL(sqlQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO - Save the database
        Log.w(TAG, "Dropping database, should save it first");
        onCreate(db);
    }
}
