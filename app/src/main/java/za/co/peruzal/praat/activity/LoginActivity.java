package za.co.peruzal.praat.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import za.co.peruzal.praat.R;
import za.co.peruzal.praat.utils.Constants;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    @Bind(R.id.nickname) EditText nickname;
    SharedPreferences preferences;
    String username;

    private Socket socket;

    {
        try {
            socket = IO.socket(Constants.Socket.URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        username = preferences.getString("username", "");
        socket.connect();

        if (!TextUtils.isEmpty(username)){
            joinUser(username);
        }
    }

    @OnClick(R.id.btnJoin) public void joinUser(){
        //TODO - Manually edit the preference and add the username
        String username = nickname.getText().toString().trim();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", username);
        editor.commit();

        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
        finish();
    }

    private void joinUser(String username){
        socket.emit(Constants.Socket.ADD_USER, username);

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Constants.USERNAME, username);
        startActivity(intent);
        finish();
    }

}
