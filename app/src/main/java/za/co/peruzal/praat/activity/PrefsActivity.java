package za.co.peruzal.praat.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import za.co.peruzal.praat.R;

/**
 * Created by joseph on 7/15/15.
 */
public class PrefsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
