package za.co.peruzal.praat.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import za.co.peruzal.praat.R;
import za.co.peruzal.praat.model.ChatMessage;

/**
 * Created by joseph on 7/15/15.
 */
public class ChatAdapter extends BaseAdapter {
    private ArrayList<ChatMessage> chatMessageArrayList;
    private Context context;

    public ChatAdapter(ArrayList<ChatMessage> chatMessageArrayList, Context context) {
        this.chatMessageArrayList = chatMessageArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return chatMessageArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatMessageArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        ViewHolder viewHolder;
        ChatMessage chatMessage = chatMessageArrayList.get(position);

        if (rowView == null){
            if (chatMessage.isSelf()) {
                rowView = View.inflate(context, R.layout.row_chat_left, null);
            }else {
                rowView = View.inflate(context, R.layout.row_chat_right, null);
            }
            viewHolder = new ViewHolder(rowView);

            rowView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) rowView.getTag();
        }



        viewHolder.username.setText(chatMessage.getUsername());
        viewHolder.message.setText(chatMessage.getMessage());

        return rowView;
    }

    static class ViewHolder{
        @Bind(R.id.username) TextView username;
        @Bind(R.id.message) TextView message;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
